# JSON to .Strings
A ruby converter for those that prefer to hold their Localizable strings in JSON. 

## Pre-reqs
// TODO: Add ruby version

## Installation
```
git clone https://gitlab.com/JamesSawley/json_to_strings.git
cd json_to_strings
```

## Usage
### Command Line
```
ruby json\_to\_strings.rb path/to/input.json path/to/output.strings
```

### XCode
// TODO: Instructions for adding to XCode

## Example
An example can be seen in the [`example`](https://gitlab.com/JamesSawley/json_to_strings/tree/master/example) directory. To get started with this, go and put some JSON into the `example.json` file, then run

```
ruby json_to_strings.rb example/example.json example/Localizable.strings
```

## License
[MIT](https://gitlab.com/JamesSawley/json_to_strings/blob/master/LICENSE)